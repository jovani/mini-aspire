<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->group(function () {
    Route::get('/me', function (Request $request) {
        return new \App\Http\Resources\UserResource($request->user());
    });

    Route::get('/loans', 'LoanController@index');
    Route::post('/loans', 'LoanController@store');

    Route::post('/pay/loan/{code}', 'PaymentController@loan');
    Route::post('/pay/invoice/{code}', 'PaymentController@invoice');
});

Route::post('/users', 'UserController@store');
