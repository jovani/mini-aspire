<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Http\Controllers\Controller;
use App\Http\Requests\InvoicePaymentRequest;
use App\Http\Requests\LoanPaymentRequest;
use App\Http\Resources\PaymentResource;

class PaymentController extends Controller
{
    public function invoice(InvoicePaymentRequest $request, $code)
    {
        $payment = $request->process();

        return new PaymentResource($payment);
    }

    public function loan(LoanPaymentRequest $request, $code)
    {
        $payment = $request->process();

        return new PaymentResource($payment);
    }
}
