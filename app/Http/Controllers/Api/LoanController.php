<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewLoanRequest;
use App\Http\Resources\LoanResource;
use App\Models\Loan;

class LoanController extends Controller
{
    public function index(Request $request)
    {
        return LoanResource::collection($request->user()->loans);
    }

    public function store(NewLoanRequest $request)
    {
        $request->merge(['user_id' => $request->user()->id]);
        $loan = Loan::create($request->all());

        $request->process($loan->id);

        return new LoanResource($loan);
    }
}
