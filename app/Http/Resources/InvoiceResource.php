<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->code,
            'due_date' => $this->due_on->format('Y-m-d'),
            'status' => $this->status,
            'total' => $this->total,
        ];
    }
}
