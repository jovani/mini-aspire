<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->code,
            'interest_rate' => $this->interest_rate,
            'fee' => $this->fee,
            'duration' => $this->duration,
            'repayment' => $this->repayment,
            'amount' => $this->amount,
            'date' => $this->created_at->toDateTimeString(),
            'paid' => !!$this->is_paid,
            'invoices' => InvoiceResource::collection($this->invoices),
        ];
    }
}
