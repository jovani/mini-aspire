<?php

namespace App\Http\Requests;

use App\Jobs\ProcessLoan;
use Illuminate\Foundation\Http\FormRequest;

class NewLoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Fields to validate.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric',
            'fee' => 'required|numeric', 
            'interest_rate' => 'required|numeric',
            'duration' => 'required|integer',
            'repayment' => 'required|in:monthly,weekly'
        ];
    }

    /**
     * Process this request.
     * 
     * @param int $id Loan ID
     */
    public function process($id)
    {
        // Should be a non-queued job to get result immediately
        ProcessLoan::dispatch($id);
    }
}
