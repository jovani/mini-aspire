<?php

namespace App\Http\Requests;

use App\Models\Invoice;
use App\Models\Payment;
use App\Models\Loan;
use Illuminate\Foundation\Http\FormRequest;

class LoanPaymentRequest extends FormRequest
{
    private function getLoan()
    {
        return Loan::where('code', $this->code)->first();
    }

    /**
     * The next payable invoice.
     */
    private function getInvoice()
    {
        return $this->getLoan()->invoices()->unPaid()->oldest('due_on')->first();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validate fields.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required'
        ];
    }

    /**
     * Making sure it is not yet paid and payment is enough to pay invoice.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $loan = $this->getLoan();
            if (!$loan) {
                $validator->errors()->add('code', 'Resource not found!');
            } elseif ($loan->is_paid) {
                    $validator->errors()->add('status', 'Loan is fully paid!');
            } elseif (($invoice = $this->getInvoice()) && $this->amount < $invoice->total) {
                $validator->errors()->add('amount', 'Please pay invoiced amount of ' . $invoice->total);
            }
        });
    }

    /**
     * Process this request.
     * Mark loan as paid if no more unpaid invoice left.
     * 
     * @return \App\Models\Payment
     */
    public function process()
    {
        $invoice = $this->getInvoice();
        $payment = $invoice->payment()->create([
            'payer_id' => $this->user()->id,
            'amount' => $this->amount,
            'reference_no' => $this->reference_no,
        ]);

        $invoice->update(['status' => Invoice::STATUS_PAID]);
        
        $loan = $this->getLoan();
        if ($loan->invoices()->unPaid()->count() === 0) {
            $loan->update(['is_paid' => 1]);
        }

        return $payment;
    }
}
