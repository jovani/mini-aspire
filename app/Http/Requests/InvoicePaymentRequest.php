<?php

namespace App\Http\Requests;

use App\Models\Invoice;
use App\Models\Payment;
use Illuminate\Foundation\Http\FormRequest;

class InvoicePaymentRequest extends FormRequest
{
    private function getInvoice()
    {
        return Invoice::where('code', $this->code)->first();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validate fields.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required'
        ];
    }

    /**
     * Making sure it is not yet paid and payment is enough to pay invoice.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $invoice = $this->getInvoice();
            if (!$invoice) {
                $validator->errors()->add('code', 'Resource not found!');
            } else {
                if ($invoice->status === Invoice::STATUS_PAID) {
                    $validator->errors()->add('status', 'Invoice is already paid!');
                }

                if ($this->amount < $invoice->total) {
                    $validator->errors()->add('amount', 'Please pay invoiced amount of ' . $invoice->total);
                }
            }
        });
    }

    /**
     * Process this request.
     * 
     * @return \App\Models\Payment
     */
    public function process()
    {
        $invoice = $this->getInvoice();
        $payment = $invoice->payment()->create([
            'payer_id' => $this->user()->id,
            'amount' => $this->amount,
            'reference_no' => $this->reference_no,
        ]);

        $invoice->update(['status' => Invoice::STATUS_PAID]);

        if ($invoice->loan->invoices()->unPaid()->count() === 0) {
            $invoice->loan->update(['is_paid' => 1]);
        }

        return $payment;
    }
}
