<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

/**
 * App\Models\Payment
 * 
 * @property int $id
 * @property string $code
 * @property int $payer_id
 * @property int $invoice_id Payment is always against an invoice
 * @property float $amount
 * @property string $reference_no External references like check #, etc.
 */
class Payment extends Model
{
    protected $fillable = [
        'payer_id',
        'invoice_id',
        'amount',
        'reference_no',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->code = (string) Uuid::generate(4);
        });
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function payer()
    {
        return $this->belongsTo(User::class, 'payer_id');
    }
}
