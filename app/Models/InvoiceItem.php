<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\InvoiceItem
 * 
 * @property int $id
 * @property int $invoice_id
 * @property string $name Descriptive name
 * @property float $amount
 */
class InvoiceItem extends Model
{
    protected $fillable = [
        'invoice_id',
        'name',
        'amount'
    ];
}
