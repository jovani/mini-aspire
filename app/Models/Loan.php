<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

/**
 * App\Models\Loan
 * 
 * @property int $id
 * @property string $code
 * @property int $user_id
 * @property float $interest_rate
 * @property float $fee
 * @property float $amount Desired or Principal amount
 * @property int $duration Loan duration in months
 * @property string $repayment Repayment frequency. Either `weekly` or `monthly`
 * @property boolean $is_paid If fully paid
 */
class Loan extends Model
{
    const REPAYMENT_WEEKLY = 'weekly';
    const REPAYMENT_MONTHLY = 'monthly';

    protected $casts = [
        'is_paid' => 'boolean',
    ]; 

    protected $fillable = [
        'user_id',
        'interest_rate',
        'fee',
        'amount',
        'duration',
        'repayment',
        'is_paid',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->code = (string) Uuid::generate(4);
        });
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
}
