<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

/**
 * App\Models\Invoice
 * 
 * @property int $id
 * @property int $loan_id
 * @property float $total Invoice Total
 * @property date $due_on Due date of this invoice
 * @property string $status Any of these -- `paid`, `unpaid`, `overdue`
 */
class Invoice extends Model
{
    const STATUS_PAID = 'paid';
    const STATUS_UNPAID = 'unpaid';
    const STATUS_OVERDUE = 'overdue';
    
    protected $fillable = [
        'loan_id',
        'total',
        'due_on',
        'status'
    ];

    protected $dates = [
        'due_on'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->code = (string) Uuid::generate(4);
        });
    }

    /**
     * Unpaid invoices
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnpaid($query)
    {
        return $query->where('status', '<>', self::STATUS_PAID);
    }

    public function items()
    {
        return $this->hasMany(InvoiceItem::class);
    }

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }
}
