<?php

namespace App\Jobs;

use App\Models\Invoice;
use App\Models\Loan;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Generates the necessary invoice and items for a loan.
 * Dont have to be queued to generate calculations instantly (in sync).
 */
class ProcessLoan
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;

    /**
     * Create a new job instance.
     *
     * @param int $id The loan ID
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Create invoices for each repayment schedule (based on duration and repayment freq).
     *
     * @return void
     */
    public function handle()
    {
        $loan = Loan::findOrFail($this->id);

        $startDate = Carbon::now()->startOfDay();
        $endDate = Carbon::now()->addMonths($loan->duration)->endOfDay();

        $totalRepayments = $loan->repayment === Loan::REPAYMENT_WEEKLY ? $startDate->diffInWeeks($endDate) : $loan->duration;
        $loan->repayment === Loan::REPAYMENT_WEEKLY ? $startDate->addWeek() : $startDate->addMonth();
        while ($startDate < $endDate) {
            $total = 0; // update later
            $invoice = Invoice::create([
                'loan_id' => $loan->id,
                'status' => Invoice::STATUS_UNPAID,
                'due_on' => $startDate,
                'total' => $total,
            ]);

            $principal = round($loan->amount / $totalRepayments, 2);
            $invoice->items()->create([
                'name' => 'Principal Amount',
                'amount' => $principal,
            ]);
            $total += $principal;

            $interest = round($principal * $loan->interest_rate, 2);
            $invoice->items()->create([
                'name' => 'Interest',
                'amount' => $interest,
            ]);
            $total += $interest;

            $invoice->items()->create([
                'name' => 'Fee',
                'amount' => $loan->fee,
            ]);
            $total += $loan->fee;
            
            $invoice->update(['total' => $total]);
            $loan->repayment === Loan::REPAYMENT_WEEKLY ? $startDate->addWeek() : $startDate->addMonth();
        }
    }
}
