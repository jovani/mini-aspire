## MiniAspire

**Assumptions/Limitations (due to time constraints)**
* Repayments frequency is limited to either `monthly` or `weekly`
* Payment can be made directly to an invoice or
* Payment can be made to a loan and should apply to next unpaid invoice
* Determination or tagging of invoice as overdue is yet out of scope in this version (can be done via scheduled job that checks invoices daily)
* Tested (phpunit) only the loan creation process.
