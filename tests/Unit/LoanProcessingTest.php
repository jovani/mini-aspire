<?php

namespace Tests\Unit;

use App\Jobs\ProcessLoan;
use App\Models\Loan;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoanProcessingTest extends TestCase
{
    use RefreshDatabase;
    
    public function testInvoicesAreCreatedWithMonthlyRepayments()
    {
        $user = factory(User::class)->create();
        $loan = Loan::create([
            'user_id' => $user->id,
            'interest_rate' => 0.05,
            'fee' => 15,
            'duration' => 3,
            'repayment' => Loan::REPAYMENT_MONTHLY,
            'amount' => 1000
        ]);

        ProcessLoan::dispatch($loan->id);

        // Should expect 3 invoices (1 for each month (repayment) with 3 months duration)
        $this->assertEquals(3, $loan->invoices()->count());
        
        // Total (same for all invoice)
        $invoiceTotal = round(1000 / 3 * (1.05) + 15, 2); // monthly + interest + fee
        $this->assertEquals($invoiceTotal, $loan->invoices()->inRandomOrder()->first()->total);
    }

    public function testInvoicesAreCreatedWithWeeklyRepayments()
    {
        $user = factory(User::class)->create();
        $loan = Loan::create([
            'user_id' => $user->id,
            'interest_rate' => 0.05,
            'fee' => 15,
            'duration' => 3,
            'repayment' => Loan::REPAYMENT_WEEKLY,
            'amount' => 1000
        ]);

        ProcessLoan::dispatch($loan->id);

        // We need actual number of weeks from now to 3 months in the future
        $weeks = Carbon::now()->diffInWeeks(Carbon::now()->addMonths(3));
        $this->assertEquals($weeks, $loan->invoices()->count());

        // Total (same for all invoice)
        $invoiceTotal = round(1000 / $weeks * (1.05) + 15, 2); // weekly + interest + fee
        $this->assertEquals($invoiceTotal, $loan->invoices()->inRandomOrder()->first()->total);
    }
}
